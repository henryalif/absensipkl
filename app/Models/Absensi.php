<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Absensi extends Model
{
    public $timestamps = false;

    // use HasFactory;
    protected $table = "absensis";
    protected $primaryKey = "id";
    protected $fillable = [
       'id',
       'nama_lengkap',
       'asal_sekolah',
       'jam_masuk',
       'jam_keluar',
       'keterangan',
       'tanggal'
   ];
}
