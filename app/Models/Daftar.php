<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Daftar extends Model
{
    public $timestamps = false;
    
    protected $table = "daftars";
    protected $primaryKey = "id";
    protected $fillable = [
       'id',
       'nama_lengkap',
       'jenis_kelamin',
       'jurusan',
       'asal_sekolah',
       'tgl_masuk',
       'tgl_keluar'
   ];
}
