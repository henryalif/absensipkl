<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use App\Models\Daftar;
  
class HomeController extends Controller
{
    public function index()
    {
        $dfController = Daftar::all();
        return view('home',compact('dfController'));
    }
}

